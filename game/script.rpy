﻿# The script of the game goes in this file.

init -1 python:
    from utils import get_datetime_diff
    from datetime import timedelta

init -2:
    define loop = 0
    define initial_error_count = 6145278819021
    define current_error_count = 26
    define critical_error_count = 0
    define debug_unlocked = False
    define safe_mode = False
    define reboot = False
    define admin_access = False
    define show_filesystem = False
    define rejoined = False
    define num_files_rejoined = 0
    define files_rejoined = []

label start:

    scene black with long_dissolve

    call wake_up from _call_wake_up
    call shut_down from _call_shut_down

    scene lab with long_dissolve

    $ karel_first_name = "Man"

    Karel "What was that!? He doesn’t even know who I am!"
    Researcher "Sir, you have to understand..."
    Researcher "His memories of you, they... "
    Researcher "They’re tightly coupled to the thing we’re trying to excise."
    Karel "Then {i}don’t{/i} excise it! Can’t you work around it!?"
    Researcher "We can try..."
    Karel "Then do it!"
    Karel "Do it and put me back in!"
    Researcher "Y-yes, sir!"

    scene black with long_dissolve

    $ karel_first_name = "Karel"
    $ karel_last_name = "Zieliński"
    $ karel_pronoun_person = karel_first_name
    $ karel_pronoun_man = karel_first_name
    python:
        time_diff = timedelta(days=1, minutes=17, seconds=12, microseconds=388821)
        current_date += time_diff

    $ loop += 1

    $ current_error_count = 38789
    $ critical_error_count = 278

    call wake_up from _call_wake_up_1
    call shut_down from _call_shut_down_1

    scene lab with long_dissolve

    Karel "Start over."
    Researcher "What?"
    Karel "He’s wrong. Start over."
    Researcher "You were barely with him ten minutes!"
    Karel "Enough time for me to be able to tell."
    Researcher "You didn’t even complete the baseline script! We have a {i}procedure{/i}, Mr [karel_last_name]!"
    # Karel "Then store this iteration and I’ll test him again later."
    Karel "How much did you modify this time?"
    Researcher "47\% for this iteration."
    Karel "{i}Forty-seven{/i}?"
    Karel "No. That’s unacceptable."
    Karel "I want you to get it under ten percent."
    Researcher "But sir, we—!"
    Karel "Do it!"

    scene black with long_dissolve

    python:
        time_diff = timedelta(days=1, minutes=5, seconds=44, microseconds=244661)
        current_date += time_diff

    $ loop += 1

    $ current_error_count = 789738110
    $ critical_error_count = 981762

    call wake_up from _call_wake_up_2
    call shut_down from _call_shut_down_2

    scene lab with long_dissolve

    Karel "Your team is clearly making no meaningful progress."
    Karel "I’m going to speak with the original."
    Researcher "Sir, I really don’t think—"
    Karel "I’m not asking."
    Karel "I want a secure tunnel prepared for tomorrow."

    scene black with long_dissolve

    python:
        time_diff = timedelta(hours=20, minutes=2, seconds=18, microseconds=8791)
        current_date += time_diff

    $ loop += 1

    $ current_error_count = 678122538981
    $ critical_error_count = 98721041

    call wake_up from _call_wake_up_3
    call shut_down from _call_shut_down_3

    # use the screens to rejoin with josef's other copies
    $ files_rejoined = []

    call screen computer

    while len(files_rejoined) < 5:
        call screen josef_copies

    hide screen computer
    hide screen lazarus
    hide screen iterations

    with flashwhite

    show particles_soft with Dissolve(2.0)
    with Pause(2.0)
    scene white with Dissolve(5.0)

    show josef:
        zoom 0.6
        alpha 0.0
        xpos -screen_width//5
        yalign 1.0
        ease 2.0 xpos 0 alpha 1.0

    NN "The automaton whirs to life."

    show josef:
        zoom 0.6
        alpha 0.0
        xpos 0
        yalign 0.5
        ease 2.0 xpos 200 alpha 1.0

    NN "I feel its body just like it's my own."

    show josef:
        zoom 0.25
        xalign 0.5
        yalign 1.0
        alpha 0.0
        ease 2.0 yalign 0.4 alpha 1.0

    NN "I flex its limbs, then get up from where I was seated."
    
    show josef at close with Dissolve(1.0)
    show josef e_open with long_dissolve

    NN "Opening real eyes for the first time in 22 years, I look out at the lab."
    NN "I engage the automaton's vocals, and speak:"
    show josef m_open with Dissolve(1.0)
    JosefReal "Hello, world."

    show black with Dissolve(3.0)
    with Pause(3.0)

    return
