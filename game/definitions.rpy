
init offset = -2

init python:
    from datetime import datetime
    start_date = datetime(2070, 7, 21, 19, 56, 33, 756927)
    end_date = datetime(2072, 3, 4, 13, 12, 48, 148812)
    current_date = datetime(2094, 6, 24, 19, 15, 11, 487728)

# IMAGES/FX
image white = Solid("#fff")
image black = Solid("#000")
image white50 = Color("#fff", alpha=0.5)
image black50 = Color("#000", alpha=0.5)

image ctc:
    Text("_", size=33)
    alpha 1.0
    0.5
    alpha 0
    0.5
    repeat

image ctc_nvl:
    Text("_", size=33, outlines=[ (absolute(4), "#555", 1, 1) ])
    alpha 1.0
    0.5
    alpha 0
    0.5
    repeat

# CHARACTERS
define N = Character(
    None,
    kind=nvl,
    ctc="ctc_nvl",
    # ctc_pause="ctc_pause",
    # ctc_position="fixed"
)
define NN = Character(
    None,
    ctc="ctc",
    # ctc_pause="ctc_pause",
    # ctc_position="fixed"
)
define char_nvl = Character(
    None,
    kind=nvl,
    what_suffix='”',
    what_prefix='“',
    ctc="ctc_nvl",
    # ctc_pause="ctc_pause",
    # ctc_position="fixed"
)
define char_default = Character(
    None,
    what_suffix='”',
    what_prefix='“',
    ctc="ctc",
    # ctc_pause="ctc_pause",
    # ctc_position="fixed"
)

define karel_first_name = "S2FyZWwK"
define karel_last_name = "WmVsaW5za2kK"
define karel_pronoun_person = "this person"
define karel_pronoun_man = "the man"

define KarelNVL = Character("[karel_first_name]", kind=char_nvl)
define Karel = Character("[karel_first_name]", kind=char_default)
define Researcher = Character("Researcher", kind=char_default)
define Josef = Character("Josef", kind=char_nvl)
define JosefReal = Character("Josef", kind=char_default)
define System = Character("admin >")

define SomeoneNVL = Character("???", kind=char_nvl)
define Someone = Character("???", kind=char_default)

# EFFECTS/TRANSITIONS
define dissolve = Dissolve(0.125)
define short_dissolve = Dissolve(0.25)
define long_dissolve = Dissolve(0.5)
define fade = Fade(0.5, 0.25, 0.5)
define long_fade = Fade(1.0, 0.5, 1.0)

## EFFECTS
define flashwhite = Fade(0.1, 0.0, 0.4, color="#fff")
define quick_flashwhite = Fade(0.1, 0.0, 0.1, color="#fff")
define flashred = Fade(0.1, 0.0, 0.4, color="#ff0000")
define flashblack = Fade(0.1, 0.0, 0.4, color="#000")
define quick_flashblack = Fade(0.1, 0.0, 0.1, color="#000")

# TRANSFORMS
define default_ypos = int(config.screen_height * 1.85)
define side_zoom = 0.33
define default_zoom = 0.33
define close_zoom = 0.55
define vclose_zoom = 1.0
define default_yalign = 0.6

transform middle:
    zoom default_zoom
    xoffset 0
    yoffset 100
    xalign 0.5
    yalign default_yalign

transform close:
    zoom close_zoom
    xoffset 0
    yoffset 100
    xpos screen_width//3*2
    yalign 0.1

transform v_close:
    zoom vclose_zoom
    xoffset 0
    yoffset 100
    xalign 0.5
    yalign 0.15

transform right:
    zoom default_zoom
    xoffset 0
    yoffset 100
    xalign 0.75
    yalign default_yalign

transform far_right:
    zoom default_zoom
    xoffset 0
    yoffset 100
    xalign 0.8
    yalign default_yalign

transform left:
    zoom default_zoom
    xoffset 0
    yoffset 100
    xalign 0.25
    yalign default_yalign
