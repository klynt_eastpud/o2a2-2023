
# Particles

init -2 python:
    from particle import particle_generator

image particle_soft:
    "particle"
    additive True
    zoom 3.0
    alpha 1.0
    yalign 0.5
    ease 2.0 alpha 0.0

define speed = (-800, 800)
define minspeed = 200
define count = 200
define particles_radius = 1200
define particles_count = 100
define radius = 1200

image particles_soft = particle_generator(
    "particle_soft",
    pos=(screen_width // 2 - 150, screen_height // 2 - 150),
    radius=radius,
    count=count,
    start=0,
    uniform=False,
    minspeed=minspeed,
    speed=speed)
