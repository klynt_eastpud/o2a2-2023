
## Say screen ##################################################################
##
## The say screen is used to display dialogue to the player. It takes two
## parameters, who and what, which are the name of the speaking character and
## the text to be displayed, respectively. (The who parameter can be None if no
## name is given.)
##
## This screen must create a text displayable with id "what", as Ren'Py uses
## this to manage text display. It can also create displayables with id "who"
## and id "window" to apply style properties.
##
## https://www.renpy.org/doc/html/screen_special.html#say

define saybox_border = 10
define saybox_ysize = 200
define saybox_xsize = screen_width // 3 * 2
define namebox_xsize = saybox_xsize + (saybox_border*2) - (window_bevel*2)
define saybox_xpos = (screen_width - saybox_xsize) // 2
define saybox_ypos = screen_height - saybox_ysize - 100

screen say(who, what):
    style_prefix "say"

    use quick_menu

    # outer shadow
    frame:
        background "#eee"
        ypos saybox_ypos - menu_bar_ysize - saybox_border - window_top_padding - window_bevel
        xpos saybox_xpos - saybox_border - window_bevel
        xsize saybox_xsize + (saybox_border*2) + window_bevel
        ysize saybox_ysize + (saybox_border*2) + window_top_padding + menu_bar_ysize + window_bevel

    frame:
        background "#333"
        ypos saybox_ypos - menu_bar_ysize - saybox_border - window_top_padding
        xpos saybox_xpos - saybox_border
        xsize saybox_xsize + (saybox_border*2) + window_bevel
        ysize saybox_ysize + (saybox_border*2) + window_top_padding + menu_bar_ysize

    # base
    frame:
        background "#aaa"
        ypos saybox_ypos - menu_bar_ysize - saybox_border - window_top_padding
        xpos saybox_xpos - saybox_border
        xsize saybox_xsize + (saybox_border*2)
        ysize saybox_ysize + (saybox_border*2) + window_top_padding + menu_bar_ysize

    # menu bar
    frame:
        background "#000080"
        ypos saybox_ypos - menu_bar_ysize - saybox_border - window_top_padding + window_bevel
        xpos saybox_xpos - saybox_border + window_bevel
        xsize namebox_xsize
        ysize menu_bar_ysize

        if who is not None:
            text who id "who" color "#fff"


    # inner shadow
    frame:
        background "#333"
        ypos saybox_ypos - window_bevel
        xpos saybox_xpos - window_bevel
        xsize saybox_xsize + window_bevel
        ysize saybox_ysize + window_bevel

    frame:
        background "#eee"
        ypos saybox_ypos
        xpos saybox_xpos
        xsize saybox_xsize + window_bevel
        ysize saybox_ysize + window_bevel

    window:
        style_prefix "title"
        ypos saybox_ypos
        xpos saybox_xpos
        xsize saybox_xsize
        ysize saybox_ysize
        padding (25, 25)
        background "#000"

        text what id "what" color "#fff"

    ## If there's a side image, display it above the text. Do not display on the
    ## phone variant - there's no room.
    if not renpy.variant("small"):
        add SideImage() xalign 0.0 yalign 1.0


## Make the namebox available for styling through the Character object.
init python:
    config.character_id_prefixes.append('namebox')

style say_label is default
style say_frame is empty
style say_dialogue is default
style say_thought is say_dialogue

style say_frame:
    top_padding 5

style say_label:
    ## The position, width, and alignment of the label giving the name of the
    ## speaking character.
    xpos 10
    xanchor 0
    yanchor 0.5
    xsize None
    xalign 0.0
    yalign 0.5

style say_dialogue:

    ## The maximum width of dialogue text, in pixels.
    xsize saybox_xsize - 100

    ## The horizontal alignment of the dialogue text. This can be 0.0 for left-
    ## aligned, 0.5 for centered, and 1.0 for right-aligned.
    xalign 0.0

    adjust_spacing False
