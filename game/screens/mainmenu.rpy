
## Main Menu screen ############################################################
##
## Used to display the main menu when Ren'Py starts.
##
## https://www.renpy.org/doc/html/screen_special.html#main-menu

init -3:
    image ctc_title:
        "ctc"
        zoom 2.2
        yoffset 15
        xoffset -10

    image desktop = Solid("#008080")

    define window_border = 15
    define window_top_padding = 10
    define window_bevel = 3
    define window_xpos = 250
    define window_ypos = 120
    define window_xsize = screen_width // 5 * 4
    define window_ysize = screen_height // 5 * 4
    define menu_bar_xsize = window_xsize + (window_border*2) - (window_bevel*2)
    define menu_bar_ysize = 40
    define close_button_xsize = window_border + window_border // 2

screen big_window(menu_bg="#000"):

    # outer shadow
    frame:
        background "#eee"
        ypos window_ypos - menu_bar_ysize - window_border - window_top_padding - window_bevel
        xpos window_xpos - window_border - window_bevel
        xsize window_xsize + (window_border*2) + window_bevel
        ysize window_ysize + (window_border*2) + window_top_padding + menu_bar_ysize + window_bevel

    frame:
        background "#333"
        ypos window_ypos - menu_bar_ysize - window_border - window_top_padding
        xpos window_xpos - window_border
        xsize window_xsize + (window_border*2) + window_bevel
        ysize window_ysize + (window_border*2) + window_top_padding + menu_bar_ysize + window_bevel

    # base
    frame:
        background "#aaa"
        ypos window_ypos - menu_bar_ysize - window_border - window_top_padding
        xpos window_xpos - window_border
        xsize window_xsize + (window_border*2)
        ysize window_ysize + (window_border*2) + window_top_padding + menu_bar_ysize

    # menu bar
    frame:
        background "#000080"
        ypos window_ypos - menu_bar_ysize - window_border - window_top_padding + window_bevel
        xpos window_xpos - window_border + window_bevel
        xsize window_xsize + (window_border*2) - (window_bevel*2)
        ysize menu_bar_ysize

        text "Command prompt"

    # inner shadow
    frame:
        background "#333"
        ypos window_ypos - window_bevel
        xpos window_xpos - window_bevel
        xsize window_xsize + window_bevel
        ysize window_ysize + window_bevel

    frame:
        background "#eee"
        ypos window_ypos
        xpos window_xpos
        xsize window_xsize + window_bevel
        ysize window_ysize + window_bevel

    window:
        style_prefix "title"
        ypos window_ypos
        xpos window_xpos
        xsize window_xsize
        ysize window_ysize
        padding (50, 100)
        background menu_bg

        transclude

screen main_menu():

    ## This ensures that any other menu screen is replaced.
    tag menu

    add "desktop"

    ## The use statement includes another screen inside this one. The actual
    ## contents of the main menu are in the navigation screen.
    # use navigation

    window:
        style_prefix "desktop"
        yalign 0.5
        xalign 0.5
        xsize screen_width
        ysize screen_height

        use navigation
        use big_window:

            vbox:
                spacing 300
                text "> [config.name!t] {image=ctc_title}":
                    size 75
                    style "main_menu_title"

                textbutton _("Start") action Start():
                    background "#fff"
                    hover_background "#0827F5"
                    text_color "#000"
                    text_hover_color "#fff"
                    text_size 75
                    xsize window_xsize - 100
                    padding (10, 15)


style title_window is window

style main_menu_text is gui_text
style main_menu_title is main_menu_text
style main_menu_version is main_menu_text
style main_menu_window is empty
