
## Help screen #################################################################
##
## A screen that gives information about key and mouse bindings. It uses other
## screens (keyboard_help, mouse_help, and gamepad_help) to display the actual
## help.

screen help():

    tag menu

    default device = "keyboard"

    use game_menu(_("Help")):

        style_prefix "help"

        window:
            background "#000080"
            xsize window_xsize
            xpos -50
            ypos -100

            hbox:
                style_prefix "help_nav"

                button action SetScreenVariable("device", "keyboard"):
                    text _("Keyboard")
                button action SetScreenVariable("device", "mouse"):
                    text _("Mouse")

                if GamepadExists():
                    textbutton _("Gamepad") action SetScreenVariable("device", "gamepad")

        window:
            # background "#444"
            ysize window_ysize - 150

            viewport:
                yinitial 0.0
                scrollbars "vertical"
                mousewheel True
                draggable True
                pagekeys True

                side_yfill True

                vbox:
                    if device == "keyboard":
                        use keyboard_help
                    elif device == "mouse":
                        use mouse_help
                    elif device == "gamepad":
                        use gamepad_help


screen keyboard_help():

    style_prefix "help_detail"

    window:
        has fixed:
            yfit True
        label _("Enter")
        text _("Advances dialogue and activates the interface.")

    window:
        has fixed:
            yfit True
        label _("Space")
        text _("Advances dialogue without selecting choices.")

    window:
        has fixed:
            yfit True
        label _("Arrow Keys")
        text _("Navigate the interface.")

    window:
        has fixed:
            yfit True
        label _("Escape")
        text _("Accesses the game menu.")

    window:
        has fixed:
            yfit True
        label _("Ctrl")
        text _("Skips dialogue while held down.")

    window:
        has fixed:
            yfit True
        label _("Tab")
        text _("Toggles dialogue skipping.")

    window:
        has fixed:
            yfit True
        label _("Page Up")
        text _("Rolls back to earlier dialogue.")

    window:
        has fixed:
            yfit True
        label _("Page Down")
        text _("Rolls forward to later dialogue.")

    window:
        has fixed:
            yfit True
        label "H"
        text _("Hides the user interface.")

    window:
        has fixed:
            yfit True
        label "S"
        text _("Takes a screenshot.")

    window:
        has fixed:
            yfit True
        label "V"
        text _("Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}.")

    window:
        has fixed:
            yfit True
        label "Shift+A"
        text _("Opens the accessibility menu.")


screen mouse_help():

    style_prefix "help_detail"

    window:
        has fixed:
            yfit True
        label _("Left Click")
        text _("Advances dialogue and activates the interface.")

    window:
        has fixed:
            yfit True
        label _("Middle Click")
        text _("Hides the user interface.")

    window:
        has fixed:
            yfit True
        label _("Right Click")
        text _("Accesses the game menu.")

    window:
        has fixed:
            yfit True
        label _("Mouse Wheel Up\nClick Rollback Side")
        text _("Rolls back to earlier dialogue.")

    window:
        has fixed:
            yfit True
        label _("Mouse Wheel Down")
        text _("Rolls forward to later dialogue.")


screen gamepad_help():

    style_prefix "help_detail"

    window:
        has fixed:
            yfit True
        label _("Right Trigger\nA/Bottom Button")
        text _("Advances dialogue and activates the interface.")

    window:
        has fixed:
            yfit True
        label _("Left Trigger\nLeft Shoulder")
        text _("Rolls back to earlier dialogue.")

    window:
        has fixed:
            yfit True
        label _("Right Shoulder")
        text _("Rolls forward to later dialogue.")


    window:
        has fixed:
            yfit True
        label _("D-Pad, Sticks")
        text _("Navigate the interface.")

    window:
        has fixed:
            yfit True
        label _("Start, Guide")
        text _("Accesses the game menu.")

    window:
        has fixed:
            yfit True
        label _("Y/Top Button")
        text _("Hides the user interface.")

    textbutton _("Calibrate") action GamepadCalibrate()

style help_detail_window is history_window
style help_detail_label is history_name
style help_detail_label_text is history_name_text
style help_nav_text is navigation_text
style help_nav_button is navigation_button
style help_detail_text is history_text

style help_detail_label:
    # background "#444"
    xpos gui.help_name_width
    xsize gui.help_name_width

style help_nav_button:
    xsize 200
    idle_background "#000080"
    selected_idle_background "#fff"
    hover_background "#fff"

style help_nav_text:
    hover_color "#000"
    idle_color "#fff"
    selected_idle_color "#000"

style help_detail_label_text:
    min_width gui.help_name_width

style help_detail_text:
    xpos gui.help_name_width
    xsize (int(screen_width / 3) * 2) - gui.help_name_width - 60
    min_width (int(screen_width / 3) * 2) - gui.help_name_width - 60
