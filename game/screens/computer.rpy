
screen computer():

    tag menu
    modal True
    predict False

    style_prefix "computer"

    use game_menu(_("Explorer"), show_return=False, menu_bg="#fff"):

        style_prefix "computer"

        frame:
            background "#fff"

            vbox:
                spacing 30
                hbox:
                    image "gui/directory_network_conn.png"
                    textbutton "Lazarus":
                        if not admin_access:
                            action Show("error", message="You do not have permission to access this")
                        else:
                            action Show("lazarus")
                hbox:
                    image "gui/directory_closed.png"
                    textbutton "Builds" action Show("documents")
                hbox:
                    image "gui/executable_gear.png"
                    textbutton "Settings" action Show("settings")

screen lazarus():

    modal True
    predict False

    style_prefix "computer"

    use game_menu(_("Explorer"), show_return=False, menu_bg="#fff"):

        style_prefix "computer"

        frame:
            background "#fff"

            vbox:
                spacing 30
                textbutton "^ Up one level" action Hide("lazarus")

                hbox:
                    image "gui/directory_closed.png"
                    textbutton "Iterations" action Show("iterations")

screen iterations():

    modal True
    predict False

    style_prefix "computer"

    use game_menu(_("Explorer"), show_return=False, menu_bg="#fff"):

        frame:
            background "#fff"

            vbox:
                spacing 30
                textbutton "^ Up one level" action Hide("iterations")

                hbox:
                    image "gui/directory_closed.png"
                    textbutton "Josef" action Show("josef_copies")

                hbox:
                    image "gui/directory_closed.png"
                    textbutton "Damon" action Show("copies", num=4)

                hbox:
                    image "gui/directory_closed.png"
                    textbutton "Marius" action Show("copies", num=4)

                hbox:
                    image "gui/directory_closed.png"
                    textbutton "Sulla" action Show("copies", num=4)

                hbox:
                    image "gui/directory_closed.png"
                    textbutton "Helena" action Show("copies", num=4)

screen copies(num=4):

    modal True
    predict False

    style_prefix "computer"

    use game_menu(_("Explorer"), show_return=False, menu_bg="#fff"):

        frame:
            background "#fff"

            vbox:
                spacing 30
                textbutton "^ Up one level" action Hide("copies")

                for i in range(1, num):
                    $ num = str(i).zfill(4)
                    hbox:
                        image "gui/file.png"
                        text "iteration_[num]"

screen settings():

    modal True
    predict False

    style_prefix "computer"

    use game_menu(_("Explorer"), show_return=False, menu_bg="#fff"):

        style_prefix "computer"

        frame:
            background "#fff"

            vbox:
                spacing 30
                textbutton "^ Up one level" action Hide("settings")

                hbox:
                    image "gui/file_gear.png"
                    text "registry"

screen empty_directory():

    modal True
    predict False

    style_prefix "computer"

    use game_menu(_("Explorer"), show_return=False, menu_bg="#fff"):

        style_prefix "computer"

        frame:
            background "#fff"

            vbox:
                spacing 30
                textbutton "^ Up one level" action Hide("empty_directory")
                text "No items"

style computer_text is gui_text

style computer_text:
    color "#000"
    yoffset 5

style computer_hbox:
    spacing 15

style computer_button:
    ysize 32
    background "#ccc"
    hover_background "#000"

style computer_button_text:
    color "#000"
    hover_color "#fff"