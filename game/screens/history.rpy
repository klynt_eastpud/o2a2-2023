
## History screen ##############################################################
##
## This is a screen that displays the dialogue history to the player. While
## there isn't anything special about this screen, it does have to access the
## dialogue history stored in _history_list.
##
## https://www.renpy.org/doc/html/history.html

screen history():

    tag menu

    ## Avoid predicting this screen, as it can be very large.
    predict False

    use game_menu(_("History")):

        window:

            viewport:
                # xsize int(screen_width / 3) * 2
                yinitial 1.0
                scrollbars "vertical"
                mousewheel True
                draggable True
                pagekeys True

                side_yfill True

                vbox:

                    style_prefix "history"

                    for h in _history_list:

                        window:
                            # background "#444"

                            ## This lays things out properly if history_height is None.
                            has fixed:
                                yfit True

                            if h.who:

                                label h.who:
                                    style "history_name"
                                    substitute False

                                    ## Take the color of the who text from the Character, if
                                    ## set.
                                    if "color" in h.who_args:
                                        text_color h.who_args["color"]

                            $ what = renpy.filter_text_tags(h.what, allow=gui.history_allow_tags)
                            text what:
                                substitute False

                    if not _history_list:
                        label _("The dialogue history is empty.")


## This determines what tags are allowed to be displayed on the history screen.

define gui.history_allow_tags = { "alt", "noalt" }


style history_window is empty

style history_name is gui_label
style history_name_text is gui_label_text
style history_text is gui_text

style history_label is gui_label
style history_label_text is gui_label_text

style history_window:
    xfill True
    ysize gui.history_height
    bottom_padding 25

style history_name:
    xpos gui.history_name_width
    xsize gui.history_name_width
    xanchor 1.0
    ypos 0

style history_name_text:
    min_width gui.history_name_width
    text_align 1.0

style history_text:
    xpos gui.history_name_width
    xoffset 50
    ypos 3
    xanchor 0.0
    xsize (int(screen_width / 3) * 2) - gui.history_name_width - 60
    min_width (int(screen_width / 3) * 2) - gui.history_name_width - 60
    text_align 0.0
    # layout "subtitle"
    layout "tex"

style history_label:
    xfill True

style history_label_text:
    xalign 0.5
