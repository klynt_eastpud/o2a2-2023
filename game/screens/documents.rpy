
screen documents():

    tag menu
    predict False

    style_prefix "computer"

    use game_menu(_("Explorer"), show_return=False, menu_bg="#fff"):

        frame:
            background "#fff"

            vbox:
                spacing 30
                textbutton "^ Up one level" action Show("computer")

                hbox:
                    image "gui/directory_closed.png"
                    textbutton "Josef" action Show("josef_docs")

                hbox:
                    image "gui/directory_closed.png"
                    textbutton "Damon" action Show("docs", collated=3, logs=6)

                hbox:
                    image "gui/directory_closed.png"
                    textbutton "Marius" action Show("docs", collated=2, logs=4)

                hbox:
                    image "gui/directory_closed.png"
                    textbutton "Sulla" action Show("docs", collated=1, logs=2)

                hbox:
                    image "gui/directory_closed.png"
                    textbutton "Helena" action Show("docs", collated=4, logs=7)

screen josef_docs():

    modal True
    predict False

    style_prefix "computer"

    use game_menu(_("Explorer"), show_return=False, menu_bg="#fff"):

        frame:
            background "#fff"

            vbox:
                spacing 30
                textbutton "^ Up one level" action Hide("josef_docs")

                grid 3 10:
                    transpose True
                    xspacing 80
                    yspacing 30

                    hbox:
                        image "gui/executable.png"
                        text "shell"

                    hbox:
                        image "gui/file_gear.png"
                        textbutton "configuration" action Show("josef_config")

                    for i in range(1, 14):
                        $ num = str(i).zfill(3)
                        hbox:
                            image "gui/file.png"
                            text "collated_[num]"
                    for i in range(1, 16):
                        $ num = str(i).zfill(3)
                        hbox:
                            image "gui/file.png"
                            text "research_logs_[num]"

screen docs(collated=2, logs=5):

    modal True
    predict False

    style_prefix "computer"

    use game_menu(_("Explorer"), show_return=False, menu_bg="#fff"):

        frame:
            background "#fff"

            vbox:
                spacing 30
                textbutton "^ Up one level" action Hide("docs")

                hbox:
                    image "gui/executable.png"
                    text "shell"

                hbox:
                    image "gui/file_gear.png"
                    text "configuration"

                for i in range(1, collated):
                    $ num = str(i).zfill(3)
                    hbox:
                        image "gui/file.png"
                        text "collated_[num]"
                for i in range(1, logs):
                    $ num = str(i).zfill(3)
                    hbox:
                        image "gui/file.png"
                        text "research_logs_[num]"
