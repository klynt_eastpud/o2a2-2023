
init python:

    def file_dragged(drags, drop):

        if not drop:
            return

        files_rejoined.append(drags[0].drag_name)

        return True

init -3:

    style filesystem_frame is empty

    style filesystem_frame:
        xalign 0.5
        xsize int(screen_width / 3) * 2

    style filesystem_hbox:
        spacing 15

    style filesystem_window:
        background "#555"
        xpadding 5
        ypadding 5

screen josef_copies():

    modal True
    predict False

    style_prefix "computer"

    use game_menu(_("Explorer"), show_return=False, menu_bg="#fff"):

        frame:
            background "#fff"
            textbutton "^ Up one level" action Hide("josef_copies")

            $ tooltip = GetTooltip()

            if tooltip:

                nearrect:
                    focus "tooltip"
                    prefer_top True

                    frame:
                        xalign 0.5
                        image "gui/move.png"

            draggroup:
                style_prefix "draggable"
                drag:
                    drag_name "josef_master"
                    draggable False
                    ypos 62
                    xpos 100
                    hbox:
                        spacing 5
                        image "gui/directory_closed.png"
                        textbutton "MASTER" action Show("master_folder")

                if "josef1" not in files_rejoined:
                    drag:
                        drag_name "josef1"
                        droppable False
                        dragged file_dragged
                        ypos 150
                        xpos 300
                        hbox:
                            image "gui/move.png"
                            image "gui/file.png"
                            text "iteration_001_099"

                if "josef2" not in files_rejoined:
                    drag:
                        drag_name "josef2"
                        droppable False
                        dragged file_dragged
                        ypos 200
                        xpos 350
                        hbox:
                            image "gui/move.png"
                            image "gui/file.png"
                            text "iteration_100_199"

                if "josef3" not in files_rejoined:
                    drag:
                        drag_name "josef3"
                        droppable False
                        dragged file_dragged
                        ypos 250
                        xpos 0
                        hbox:
                            image "gui/move.png"
                            image "gui/file.png"
                            text "iteration_200_299"

                if "josef4" not in files_rejoined:
                    drag:
                        drag_name "josef4"
                        droppable False
                        dragged file_dragged
                        ypos 300
                        xpos 500
                        hbox:
                            image "gui/move.png"
                            image "gui/file.png"
                            text "iteration_300_399"

                if "josef5" not in files_rejoined:
                    drag:
                        drag_name "josef5"
                        droppable False
                        dragged file_dragged
                        ypos 350
                        xpos 120
                        hbox:
                            image "gui/move.png"
                            image "gui/file.png"
                            text "iteration_400_499"

style draggable_window is empty
style draggable_frame is empty

style draggable_button:
    ysize 32
    background "#ccc"
    hover_background "#000"

style draggable_text:
    color "#000"
    background "#fff"

style draggable_button_text:
    color "#000"
    hover_color "#fff"
