
## Confirm screen ##############################################################
##
## The confirm screen is called when Ren'Py wants to ask the player a yes or no
## question.
##
## https://www.renpy.org/doc/html/screen_special.html#confirm

define confirm_window_xsize = 700
define confirm_window_ysize = 300
define confirm_window_ypos = screen_height // 2 - (confirm_window_ysize//2)
define confirm_window_xpos = screen_width // 2 - (confirm_window_xsize//2)
define confirm_menu_bar_xsize = confirm_window_xsize - (window_bevel*2)

screen confirm(message, yes_action, no_action):

    ## Ensure other screens do not get input while this screen is displayed.
    modal True

    zorder 200

    style_prefix "confirm"

    add "black50"

    # outer shadow
    frame:
        background "#eee"
        ypos confirm_window_ypos - window_bevel
        xpos confirm_window_xpos - window_bevel
        xsize confirm_window_xsize + window_bevel
        ysize confirm_window_ysize + window_bevel

    frame:
        background "#333"
        ypos confirm_window_ypos
        xpos confirm_window_xpos
        xsize confirm_window_xsize + window_bevel
        ysize confirm_window_ysize + window_bevel

    # base
    frame:
        background "#aaa"
        ypos confirm_window_ypos
        xpos confirm_window_xpos
        xsize confirm_window_xsize
        ysize confirm_window_ysize

    window:
        ypos confirm_window_ypos
        xpos confirm_window_xpos
        xsize confirm_window_xsize
        ysize confirm_window_ysize
        padding (50, 90)

        vbox:
            spacing 50
            xalign 0.5
            text _(message):
                color "#000"
                xalign 0.5
                text_align 0.5

            hbox:
                style_prefix "confirm_options"
                spacing 50
                xalign 0.5

                button action yes_action:
                    text _("Yes")
                button action no_action:
                    text _("No")

    # menu bar
    frame:
        background "#000080"
        ypos confirm_window_ypos + window_bevel
        xpos confirm_window_xpos + window_bevel
        xsize confirm_menu_bar_xsize
        ysize menu_bar_ysize
        padding (5, 5)

        text "Confirm"

    ## Right-click and escape answer "no".
    key "game_menu" action no_action

screen error(message="Error"):

    ## Ensure other screens do not get input while this screen is displayed.
    modal True

    zorder 300

    style_prefix "confirm"

    add "black50"

    # outer shadow
    frame:
        background "#eee"
        ypos confirm_window_ypos - window_bevel
        xpos confirm_window_xpos - window_bevel
        xsize confirm_window_xsize + window_bevel
        ysize confirm_window_ysize + window_bevel

    frame:
        background "#333"
        ypos confirm_window_ypos
        xpos confirm_window_xpos
        xsize confirm_window_xsize + window_bevel
        ysize confirm_window_ysize + window_bevel

    # base
    frame:
        background "#aaa"
        ypos confirm_window_ypos
        xpos confirm_window_xpos
        xsize confirm_window_xsize
        ysize confirm_window_ysize

    window:
        ypos confirm_window_ypos
        xpos confirm_window_xpos
        xsize confirm_window_xsize
        ysize confirm_window_ysize
        padding (50, 90)

        vbox:
            style_prefix "confirm_options"
            spacing 50
            xalign 0.5
            text _(message):
                color "#000"
                xalign 0.5
                text_align 0.5
            button action Hide("error"):
                xalign 0.5
                text _("OK")

    # menu bar
    frame:
        background "#000080"
        ypos confirm_window_ypos + window_bevel
        xpos confirm_window_xpos + window_bevel
        xsize confirm_menu_bar_xsize
        ysize menu_bar_ysize
        padding (5, 5)
        hbox:
            spacing 5
            image "gui/error.png"
            text "Error" yoffset 5 xoffset 5

    ## Right-click and escape answer "no".
    key "game_menu" action Hide("error")


style confirm_frame is empty
style confirm_prompt is gui_prompt
style confirm_prompt_text is gui_prompt_text
style confirm_options_button is gui_medium_button
style confirm_options_text is gui_medium_button_text

style confirm_prompt_text:
    text_align 0.5
    color "#000"
    layout "subtitle"

style confirm_options_button:
    background "#000"
    hover_background "#fff"
    xpadding 15
    ypadding 10
    xsize 130

style confirm_options_text:
    xalign 0.5
    idle_color "#fff"
    hover_color "#000"
