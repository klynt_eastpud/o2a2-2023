
## Game Menu screen ############################################################
##
## This lays out the basic common structure of a game menu screen. It's called
## with the screen title, and displays the background, title, and navigation.
##
## The scroll parameter can be None, or one of "viewport" or "vpgrid". When
## this screen is intended to be used with one or more children, which are
## transcluded (placed) inside it.

screen game_menu(title, show_return=True, menu_bg="#000"):

    style_prefix "game_menu"

    add "desktop"

    ## The use statement includes another screen inside this one. The actual
    ## contents of the main menu are in the navigation screen.
    # use navigation

    window:
        style_prefix "desktop"
        yalign 0.5
        xalign 0.5
        xsize screen_width
        ysize screen_height

        use navigation
        use big_window(menu_bg=menu_bg):

            transclude

    if show_return:
        button action Return():
            ypos window_ypos - menu_bar_ysize - window_border - window_top_padding + window_bevel
            xalign 1.0
            xpos window_xpos + window_xsize + window_border - (window_bevel*2)
            ysize menu_bar_ysize
            background "#fff"
            hover_background "#000"
            text ("Return"):
                color "#000"
                hover_color "#fff"

    use navigation

    # label title

    if main_menu:
        key "game_menu" action ShowMenu("main_menu")


style game_menu_outer_frame is empty
style game_menu_content_frame is empty
style game_menu_viewport is gui_viewport
style game_menu_side is gui_side
style game_menu_scrollbar is gui_vscrollbar

style game_menu_label is gui_label
style game_menu_label_text is gui_label_text

style game_menu_outer_frame:
    top_padding 60
    xsize screen_width
    ysize screen_height

style game_menu_content_frame:
    xalign 0.5
    xsize int(screen_width / 3) * 2
    # ypadding 100
    background "#0827F5"

style game_menu_viewport:
    xsize 1380

style game_menu_vscrollbar:
    unscrollable gui.unscrollable

style game_menu_side:
    spacing 15

style game_menu_label:
    # xpos 75
    ysize 180

style game_menu_label_text:
    size 75
    color gui.accent_color
    yalign 1.0
