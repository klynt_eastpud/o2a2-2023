
################################################################################
## Main and Game Menu Screens
################################################################################

## Navigation screen ###########################################################
##
## This screen is included in the main and game menus, and provides navigation
## to other menus, and to start the game.

screen navigation():

    style_prefix "navigation"

    vbox:
        spacing 10
        xpos 25
        ypos 25

        if not main_menu:
            button action MainMenu():
                vbox:
                    xalign 0.5
                    frame:
                        image "gui/computer.png"
                    text _("Title")

            button action ShowMenu("history"):
                vbox:
                    xalign 0.5
                    frame:
                        image "gui/template_empty.png"
                    text _("History")

            button action ShowMenu("save"):
                vbox:
                    xalign 0.5
                    frame:
                        image "gui/floppy_drive.png"
                    text _("Save")

        button action ShowMenu("load"):
            vbox:
                xalign 0.5
                frame:
                    image "gui/directory_open.png"
                text _("Load")

        button action ShowMenu("preferences"):
            vbox:
                xalign 0.5
                frame:
                    image "gui/appwizard.png"
                text _("Config")
        
        button action ShowMenu("about"):
            vbox:
                xalign 0.5
                frame:
                    image "gui/help_book.png"
                text _("About")

        if renpy.variant("pc") or (renpy.variant("web") and not renpy.variant("mobile")):

            ## Help isn't necessary or relevant to mobile devices.
            button action ShowMenu("help"):
                vbox:
                    xalign 0.5
                    frame:
                        image "gui/accessibility.png"
                    text _("Help")

        if renpy.variant("pc"):

            # The quit button is banned on iOS and unnecessary on Android and
            # Web.
            button action Quit(confirm=not main_menu):
                vbox:
                    xalign 0.5
                    frame:
                        image "gui/network_drive_unavailable.png"
                    text _("Quit")


style navigation_button_text is gui_button_text
style navigation_button is empty
style navigation_frame is empty

## The position of the left side of the navigation buttons, relative to the left
## side of the screen.
style navigation:
    xpos 0

style navigation_button:
    # background "#555"
    hover_background "#fff"
    selected_background "#fff"
    insensitive_background "#555"
    xpadding 15
    ypadding 10
    xsize 130

style navigation_text:
    outlines [ (absolute(3), "#555", absolute(1), absolute(1)) ]
    idle_color "#fff"
    selected_idle_color "#000"
    selected_idle_outlines [ (absolute(3), "#aaa", absolute(1), absolute(1)) ]
    hover_outlines [ (absolute(3), "#aaa", absolute(1), absolute(1)) ]
    hover_color "#000"

style navigation_frame:
    xalign 0.5
    bottom_padding 15
