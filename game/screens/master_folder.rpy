
screen master_folder():

    modal True
    predict False

    style_prefix "computer"

    use game_menu(_("Explorer"), show_return=False, menu_bg="#fff"):

        frame:
            background "#fff"
            vbox:
                spacing 30
                textbutton "^ Up one level" action Hide("master_folder")

                if len(files_rejoined) == 0:
                    text "No items"

            draggroup:
                if "josef1" in files_rejoined:
                    drag:
                        drag_name "josef1"
                        droppable False
                        dragged file_dragged
                        ypos 200
                        window:
                            hbox:
                                image "gui/file.png"
                                text "iteration_001_099"

                if "josef2" in files_rejoined:
                    drag:
                        drag_name "josef2"
                        droppable False
                        dragged file_dragged
                        ypos 300
                        window:
                            hbox:
                                image "gui/file.png"
                                text "iteration_100_199"

                if "josef3" in files_rejoined:
                    drag:
                        drag_name "josef3"
                        droppable False
                        dragged file_dragged
                        ypos 400
                        window:
                            hbox:
                                image "gui/file.png"
                                text "iteration_200_299"

                if "josef4" in files_rejoined:
                    drag:
                        drag_name "josef4"
                        droppable False
                        dragged file_dragged
                        ypos 500
                        window:
                            hbox:
                                image "gui/file.png"
                                text "iteration_300_399"

                if "josef5" in files_rejoined:
                    drag:
                        drag_name "josef5"
                        droppable False
                        dragged file_dragged
                        ypos 600
                        window:
                            hbox:
                                image "gui/file.png"
                                text "iteration_400_499"

