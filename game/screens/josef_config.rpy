
screen josef_config():

    modal True
    predict False

    use game_menu(_("Explorer"), show_return=False, menu_bg="#000"):

        frame:
            xsize window_xsize - 100
            background "#000"

            style_prefix "josef_config"

            vbox:
                spacing 30
                text "Save and exit for changes to take effect"
                textbutton "Save and exit" action Hide("josef_config") style "back_button"

                vbox:
                    hbox:
                        frame:
                            text "DEBUG"
                        textbutton "[debug_unlocked]" action ToggleVariable("debug_unlocked", True, False)

                    if debug_unlocked:
                        hbox:
                            frame:
                                text "SUPERUSER"
                            textbutton "[admin_access]" action ToggleVariable("admin_access", True, False)

                    hbox:
                        frame:
                            text "ROOT_DIR"
                        textbutton "HOME"

                    hbox:
                        frame:
                            text "BACKUP_API_URL"
                        textbutton "XXXXXXXXXXXXXXXXX"

                    hbox:
                        frame:
                            text "BACKUP_API_KEY"
                        textbutton "XXXXXXXXXXXXXXXXX"

                    hbox:
                        frame:
                            text "LAZARUS_API_URL"
                        textbutton "XXXXXXXXXXXXXXXXX"

                    hbox:
                        frame:
                            text "LAZARUS_API_KEY"
                        textbutton "XXXXXXXXXXXXXXXXX"

style josef_config is empty
style josef_config_frame is empty
style back_button is gui_button

style back_button:
    xsize None
    background "#fff"
    hover_background "#0827F5"

style back_button_text:
    color "#000"
    hover_color "#fff"

style josef_config_button:
    background "#555"
    hover_background "#fff"
    xsize 300

style josef_config_button_text:
    color "#fff"
    hover_color "#000"

style josef_config_text:
    yoffset 5

style josef_config_hbox:
    spacing 50

style josef_config_frame:
    xsize 300