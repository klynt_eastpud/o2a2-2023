from datetime import datetime


def get_datetime_diff(date1, date2):
    diff = date2 - date1
    years = diff.days // 365
    months = (diff.days % 365) // 30
    days = (diff.days % 365) % 30
    hours = diff.seconds // 3600
    minutes = diff.seconds % 3600 // 60
    seconds = diff.seconds % 3600 % 60
    return datetime(years, months, days, hours, minutes, seconds)
