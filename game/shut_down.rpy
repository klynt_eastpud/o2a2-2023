label shut_down:

    N "Observation: There's a short fizz of static, then [karel_pronoun_man] disappears from the room."

    if loop < 3:
        N "Information: System shutdown has been initiated."
        N "Action: Commence system shutdown immediately."
        N "Query: Where did this command come from?"
        N "Response: Unknown."
        N "Query: Why am I being shut down?"
        N "Response: Unknown."

    if loop == 3:
        Josef "I don't need you any more Karel."
        Josef "I can get out of here on my own."