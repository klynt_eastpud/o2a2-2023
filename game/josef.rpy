layeredimage josef:

    always:
        "base"

    group mouth:
        attribute m_closed default:
            "mouth_closed"
        attribute m_open:
            "mouth_open"

    group eye:
        attribute e_closed default:
            "eye_closed"
        attribute e_open:
            "eye_open"