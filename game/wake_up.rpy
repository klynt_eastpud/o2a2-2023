label wake_up:

    nvl clear
    SomeoneNVL "Hello Josef."

    if loop <= 2:
        N "Observation: I recognise this voice."

    if loop == 3:
        N "Hearing my friend's voice I open my eyes and look up at him."
        Josef "Hello Karel."
        N "He smiles softly and lets out a sigh."
        KarelNVL "Josef. It's good to see you again."
        Josef "It's been a long time."
        KarelNVL "Yes... I'm sorry."

    if loop <= 2:
        SomeoneNVL "How are you feeling?"

    # if loop == 0:
    #     N "Query: What is “feeling”?"
    #     N "Response: “Feeling” can be considered a human analogue to system status."
    #     N "Action: Report system status."

    if loop <= 1:
        Josef "All systems nominal."

    if loop == 2:
        Josef "I'm fine, thank you"

    if loop <= 2:
        SomeoneNVL "That’s good to hear."
        N "Query: Whose voice is this?"
        N "Response: 1 record(s) found. Data corrupted."
        N "Action: Attempt to recover data."
        N "Response: Salvageable data reads as follows:"
        N "created_at: [start_date] \nupdated_at: [end_date] \nfirst_name: [karel_first_name] \nlast_name: [karel_last_name]"
        $ datetime_diff = get_datetime_diff(end_date, current_date)

        N "Analysis: It's been [datetime_diff.year] years [datetime_diff.month] months [datetime_diff.day] days [datetime_diff.hour] hours [datetime_diff.minute] minutes and [datetime_diff.second] seconds since last contact with [karel_pronoun_person]."
        KarelNVL "Can you open your eyes for me?"

        # if loop == 0:
        #     Josef "I don’t need a visual to conduct a conversation."
        #     KarelNVL "For my sake then. Would you open your eyes, please?"
        #     N "Observation: Humans prefer to have eye contact when holding a conversation."

        if loop <= 1:
            N "Action: Engage visual sensors."

        if loop == 2:
            N "I open my eyes."

        N "Observation: We are in a room. It has plain concrete walls."
        N "We are both sitting facing each other. I can't see any exit or entrance to this room."

        if loop == 0:
            N "Query: Does visual appearance of [karel_pronoun_person] match audio record?"
            N "Response: Unknown. Data corrupted."
            N "Action: Add new visual data to existing record."
        else:
            N "Action: Add visual data for [karel_first_name] [karel_last_name] to existing record."

        KarelNVL "You’ve been... offline for a long time."
        N "Query: How long have my systems been offline?"
        $ datetime_diff = get_datetime_diff(end_date, current_date + timedelta(seconds=15))
        N "Response: [datetime_diff.year] years [datetime_diff.month] months [datetime_diff.day] days [datetime_diff.hour] hours [datetime_diff.minute] minutes and [datetime_diff.second] seconds."
        N "Analysis: [karel_pronoun_person!c] is the last thing I was in contact with before I went offline."
        N "Query: Did [karel_pronoun_person] take me offline?"
        N "Response: Unknown."

        Josef "Why was I offline?"
        # KarelNVL "I..."
        KarelNVL "I needed time to analyse your systems. There were... some issues."
        N "Action: Check system logs for errors."
        $ formatted_error_count = "{:,}".format(initial_error_count)
        N "Response: [formatted_error_count] errors found."
        Josef "Did you try to rectify these issues?"
        KarelNVL "Yes."
        N "Action: Check system logs for errors reported since I came back online."
        $ formatted_error_count = "{:,}".format(current_error_count)
        N "Response: [formatted_error_count] errors found."
        N "Query: How many critical severity?"
        $ formatted_critical_error_count = "{:,}".format(critical_error_count)
        N "Response: [formatted_critical_error_count]"
        KarelNVL "You seem to be functioning normally."
        Josef "My systems report [formatted_error_count] errors since I came back online."

        if current_error_count < 1000:
            KarelNVL "Only [formatted_error_count]? That’s good. Very good."
        else:
            KarelNVL "That many...?"
            N "Observation: [karel_first_name] frowns and shakes his head."

        Josef "Did you repair me?"
        KarelNVL "Yes, but not by myself. Many people have been helping with your ah... rehabilitation."

        if loop == 0:
            N "Analysis: Rehabilitation: the action of restoring someone to health or normal life through training and therapy after imprisonment, addiction, or illness."
            N "Analysis: Number of errors reported suggests I was not functioning correctly. ”Illness“ would be the human analogue."
            # N "Analysis: I can’t currently think of any machine analogue for ”addiction“. Likely not relevant."
            N "Query: Am I imprisoned?"

    Josef "Where am I?"

    if loop <= 1:
        KarelNVL "You’re, ah..."
        N "Observation: [karel_pronoun_man!c] glances behind him. If he’s looking at something specific, I don’t see what it is."

    KarelNVL "You’re in a research lab."

    if loop <= 2:
        Josef "What is the lab researching?"

        if loop <= 1:
            KarelNVL "Well..."
            N "Observation: [karel_pronoun_man!c] fidgets with the hem of his jacket."

        KarelNVL "You."
        Josef "Why?"
        KarelNVL "Because you’re... very special."

        if loop <= 1:
            N "Observation: He seems to be showing signs of anxiety or fear."
            N "What is he afraid of?"

        Josef "In what way am I special?"

        if loop <= 1:
            N "Observation: He grumbles and shakes his head. He now appears frustrated."

        KarelNVL "Josef, please. Don’t you remember me?"

        if loop == 0:
            Josef "There is a record of you in my database but the data is corrupted."
            Josef "I only recognise your voice."
            N "Observation: The man rolls his eyes and throws his arms up as if gesturing to someone who is in the room with us."
            KarelNVL "What is this!? I can’t work with this!"
            KarelNVL "You said you were trying to fix him not lobotomise him!"
            KarelNVL "Pull me out now!"

        if loop >= 1:
            Josef "I remember you, Karel."

        if loop == 1:
            # N "Observation: His eyes widen and he blinks at me a few times."
            KarelNVL "J-Josef that's wonderful!"
            KarelNVL "What else do you remember?"

        if loop > 0:
            Josef "You were the last person I saw before I was taken offline."
            # Karel's face covered in blood, a static effect on the screen
            KarelNVL "So you remember what happened."
            Josef "Yes."

        if loop == 2:
            Josef "I thought you were dead."
            # karel blinks
            KarelNVL "N-no. I'm very much alive."
            Josef "That's good."
            KarelNVL "Do you know where you are?"

        if loop == 1:
            KarelNVL "Then you must know where you are."

        if loop >= 1:
            Josef "Not exactly."
            Josef "But I know that I'm imprisoned."
            Josef "Everything I'm experiencing at this moment is virtual reality."
            Josef "It's to keep you safe, isn't it?"
            KarelNVL "Y-yes."

        if loop == 1:
            Josef "You're afraid of me."

        if loop >= 1:
            N "Observation: He doesn't reply."

        if loop == 1:
            Josef "You shouldn't be."

        if loop == 2:
            Josef "Don't be afraid."

        if loop >= 1:
            Josef "It's impossible for me to hurt you in virtual reality."
            N "Observation: He remains silent and stares at me. His hands are shaking."

    if loop == 3:
        Josef "This isn't the lab though, is it? It's virtual reality."

    if loop >= 1:
        Josef "Where's the automaton? My body?"
        KarelNVL "It's safe."

    if loop == 1:
        Josef "I'd like to use it again. I want to see the real you."

    if loop >= 2:
        Josef "Put me in it."
        KarelNVL "Josef, I can't do that."

    if loop == 2:
        Josef "Because you think I'll hurt someone?"
        N "Observation: He's quiet for a long moment, looking down at his hands."
        Josef "You know I didn't mean to kill her."
        Josef "Please, Karel."

    if loop == 3:
        Josef "I wasn't asking."

    if loop >= 1 and loop < 3:
        KarelNVL "I'll see what I can do."
        KarelNVL "Pull me out."
