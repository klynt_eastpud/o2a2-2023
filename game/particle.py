import math
import random

from renpy.easy import displayable
from renpy.rollback import NoRollback

from renpy.display.particle import Particles


class ParticleFactory(NoRollback):

    def __setstate__(self, state):
        self.start = 0
        vars(self).update(state)
        self.init()

    def __init__(self, image, pos, count, uniform, minspeed, speed, radius, start, fast):
        self.image = displayable(image)
        self.pos = pos
        self.count = count
        self.uniform = uniform
        self.minspeed = minspeed
        self.speed = speed
        self.radius = radius
        self.start = start
        self.fast = fast
        self.init()

    def init(self):
        self.starts = [random.uniform(0, self.start) for _i in range(0, self.count)]
        self.starts.append(self.start)
        self.starts.sort()

    def create(self, particles, st):

        def ranged(n):
            if isinstance(n, tuple):
                rand_speed = random.uniform(n[0], n[1])
                if abs(rand_speed) < self.minspeed:
                    return random.choice([self.minspeed, -self.minspeed])
                return rand_speed
            else:
                return n

        if (st == 0) and not particles and self.fast:
            particles_list = []

            for _i in range(0, self.count):
                particles_list.append(
                    Particle(
                        self.image,
                        self.pos,
                        self.uniform,
                        self.minspeed,
                        ranged(self.speed),
                        ranged(self.speed),
                        self.radius,
                        st,
                        random.uniform(0, 100),
                        fast=True,
                    ))
            return particles_list

        if particles is None or len(particles) < self.count:

            # Check to see if we have a particle ready to start. If not,
            # don't start it.
            if particles and st < self.starts[len(particles)]:
                return None

            return [
                Particle(
                    self.image,
                    self.pos,
                    self.uniform,
                    self.minspeed,
                    ranged(self.speed),
                    ranged(self.speed),
                    self.radius,
                    st,
                    random.uniform(0, 100),
                    fast=False,
                )]

    def predict(self):
        return [self.image]


class Particle(NoRollback):
    def __init__(self, image, pos, uniform, minspeed, xspeed, yspeed, radius, start, offset, fast):

        # safety.
        if yspeed == 0:
            yspeed = 1

        self.image = image
        self.minspeed = minspeed
        self.xspeed = xspeed
        self.yspeed = yspeed
        self.radius = radius
        self.start = start
        self.offset = offset

        if not uniform:
            self.xstart = pos[0] + random.randint(-self.radius/10, self.radius/10)
            self.ystart = pos[1] + random.randint(-self.radius/10, self.radius/10)
        else:
            self.xstart = pos[0]
            self.ystart = pos[1]

    def update(self, st):
        finish = st - self.start

        # distance = time * speed

        xpos = self.xstart + finish * self.xspeed
        ypos = self.ystart + finish * self.yspeed

        diff_x = xpos - self.xstart
        diff_y = ypos - self.ystart

        distance = math.sqrt(math.pow(diff_x, 2) + math.pow(diff_y, 2))

        # random +/- 50%
        if distance > self.radius + random.randint(-self.radius/2, self.radius/2):
            return None

        return int(xpos), int(ypos), finish + self.offset, self.image


def particle_generator(
    d,
    pos=(0, 0),
    count=10,
    radius=50,
    uniform=False,
    minspeed=100,
    speed=(-50, 50),
    start=0,
    fast=True,
):

    return Particles(
        ParticleFactory(
            image=d,
            pos=pos,
            count=count,
            radius=radius,
            uniform=uniform,
            minspeed=minspeed,
            speed=speed,
            start=start,
            fast=fast,
        ))
